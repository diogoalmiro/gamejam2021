﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmplifySound : MonoBehaviour
{

    public FMODUnity.StudioEventEmitter Monster;
    public FMODUnity.StudioEventEmitter Baby;
    public FMODUnity.StudioEventEmitter WalkieTalki;
    public FMODUnity.StudioEventEmitter MusicBox;
    public FMOD.Studio.EventInstance Music;
    public FMOD.Studio.EventInstance Tension;

    public GameObject walkieTokiBaby;
    public GameObject BabyToy;

    private bool canPress;

    // Start is called before the first frame update
    void Start()
    {
        Music = FMODUnity.RuntimeManager.CreateInstance("event:/Music/Spooky Ambient v.3");

        Music.start();
        Tension = FMODUnity.RuntimeManager.CreateInstance("event:/Ambience/Tension Strings");
        Tension.start();
        canPress = true;
    }

    // Update is called once per frame
    void Update()
    {
        // Amplify Monster Sound
        if (Input.GetKeyDown(KeyCode.Q) && canPress)
        {
            Debug.Log("Q pressed");
            Monster.Play();
            Music.setParameterByName("Music Fade", 1.0f);
            canPress = false;
        }
        if (Input.GetKeyUp(KeyCode.Q) && !Input.GetKeyDown(KeyCode.E))
        {
            Monster.Stop();
            Debug.Log("Q lifted");
            Music.setParameterByName("Music Fade", 0.0f);
            canPress = true;
        }
        
        //Amplify Baby Sound
        if (Input.GetKeyDown(KeyCode.E) && canPress)
        {
            Debug.Log("E pressed");
            Baby.Play();
            Music.setParameterByName("Music Fade", 1.0f);
            canPress = false;
        }
        if (Input.GetKeyUp(KeyCode.E) && !Input.GetKeyDown(KeyCode.Q))
        {
            Baby.Stop();
            Music.setParameterByName("Music Fade", 0.0f);
            canPress = true;
        }

        //Lure Monster
        if (Input.GetKeyDown(KeyCode.Alpha1) && canPress)
        {
            Debug.Log("1 pressed");
            canPress = false;
            WalkieTalki.Play();
            walkieTokiBaby.SetActive(true);
            walkieTokiBaby.GetComponent<Animation>().Play("WalkieUp");
            Music.setParameterByName("Music Fade", 1.0f);        
            AIController.Instance.CallMonster();
            StartCoroutine(LureMonster());
        }

        //Lure Baby
        if (Input.GetKeyDown(KeyCode.Alpha2) && canPress)
        {
            Debug.Log("2 pressed");
            canPress = false;
            MusicBox.Play();
            BabyToy.SetActive(true);
            BabyToy.GetComponent<Animation>().Play("ToyUp");
            Music.setParameterByName("Music Fade", 1.0f);        
            AIController.Instance.CallBaby();
            StartCoroutine(LureBaby());
        }

        Debug.Log("canPress: " + canPress);
    }

    internal void setDistance(float dist)
    {
        Music.setParameterByName("Tension Fade", dist);
    }

    private IEnumerator LureMonster()
    {
        yield return new WaitForSeconds(10);
        WalkieTalki.Stop();
        Music.setParameterByName("Music Fade", 0.0f);
        walkieTokiBaby.SetActive(false);
        canPress = true;
    }

    private IEnumerator LureBaby()
    {
        yield return new WaitForSeconds(10);
        MusicBox.Stop();
        Music.setParameterByName("Music Fade", 0.0f);
        BabyToy.SetActive(false);
        canPress = true;
    }
}
