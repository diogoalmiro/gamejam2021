using System;
using UnityEngine;

public class BabySoundEmitter : MonoBehaviour
{
    public void Awake()
    {
        if (this.GetComponent<Rigidbody>() == null && this.GetComponent<CharacterController>() == null) {
            Rigidbody rb = gameObject.AddComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.useGravity = false;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger Enter:"+other.name); // Todo Just for baby
        if( other.GetComponent<FMODUnity.StudioEventEmitter>() != null ){
            other.GetComponent<FMODUnity.StudioEventEmitter>().Play();
            other.GetComponent<FMODUnity.StudioEventEmitter>();
            
        }
    }
}