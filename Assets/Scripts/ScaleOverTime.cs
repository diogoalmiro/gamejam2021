﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleOverTime : MonoBehaviour
{
    public AnimationCurve scaleOverTime;
    public bool loop = false;

    private Vector3 initialScale;
    private float currTime;

    // Start is called before the first frame update
    void Start()
    {
        initialScale = transform.localScale;
        transform.localScale *= scaleOverTime.Evaluate(0);
        currTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (loop && scaleOverTime.keys[scaleOverTime.keys.Length - 1].time < currTime)
        {
            currTime = 0.0f;
        }

        currTime += Time.deltaTime;
        transform.localScale = initialScale * scaleOverTime.Evaluate(currTime);
    }
}