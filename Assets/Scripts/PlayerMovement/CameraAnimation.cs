﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour
{
    public CharacterController playerController;
    public Animation animation;

    private bool isMoving;
    private bool left;
    private bool right;

    // Start is called before the first frame update
    void Start()
    {
        left = true;
        right = false;
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (x != 0 || y != 0)
        {
            isMoving = true;
        }
        if (x == 0 && y == 0)
        {
            isMoving = false;
        }
        CameraAnimations();
    }

    void CameraAnimations()
    {
        if (playerController.isGrounded && isMoving && !animation.isPlaying)
        {
            if (left)
            {
                animation.Play("walkLeft");
                left = false;
                right = true;
            }
            if (right)
            {
                animation.Play("walkRight");
                left = true;
                right = false;
            }
        }
    }
}
