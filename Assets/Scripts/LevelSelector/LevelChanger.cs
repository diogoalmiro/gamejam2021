﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    #region Public Fields

    [Header("Fade Int/Out image")]
    public RawImage fadeOutUIImage; // Normaly all Black

    [Header("Use this to define the speed of the Fade in seconds")]
    public float fadeSpeed = 1.5f;

    private static GameObject instance;

    public enum FadeDirections
    {
        In, // Big Alpha man   =   1
        Out // Small Alpha man =   0
    }
    #endregion

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        if (!instance)
        {
            instance = gameObject;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }


    #region When scene loads for configuring the menu buttons
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Main_Menu")
        {
            Button btn_newGame = GameObject.Find("btn_start").GetComponent<Button>();
            btn_newGame.onClick.AddListener(NewGame);
        }


        else if (scene.name == "MansionSceneFirstFloor")
        {
            Button btn_quit = GameObject.Find("button_quit").GetComponent<Button>();
            Button btn_restart = GameObject.Find("button_restart").GetComponent<Button>();

            btn_quit.onClick.AddListener(ExitGame);
            btn_restart.onClick.AddListener(RestartLevel);


            GameObject.Find("GameOverScreen").SetActive(false);
        }

        fadeOutUIImage = GameObject.Find("fader").GetComponent<RawImage>();
        StartCoroutine(Fade(FadeDirections.Out));
    }

    #endregion


    #region Start Game
    public void NewGame()
    {
        StartCoroutine(GameObject.Find("hand").GetComponent<fadeImg>().FadeIn());
        
    }
    #endregion

    public void StartNewGame()
    {
        StartCoroutine(FadeAndLoadScene(FadeDirections.In, "MansionSceneFirstFloor"));
    }


    #region Return to Main Menu
    public void ReturnMainMenu()
    {
        StartCoroutine(FadeAndLoadScene(FadeDirections.In, "Main_Menu"));
    }
    #endregion


    #region Restart Level
    public void RestartLevel()
    {
        StartCoroutine(FadeAndLoadScene(FadeDirections.In, SceneManager.GetActiveScene().name));
    }
    #endregion

    #region Exit the game
    public void ExitGame()
    {
        StartCoroutine(FadeAndLoadScene(FadeDirections.In, "Exit"));
    }
    #endregion




    #region FADE
    private IEnumerator Fade(FadeDirections fadeDirection)
    {
        float alpha = (fadeDirection == FadeDirections.Out) ? 1 : 0;
        float fadeEndValue = (fadeDirection == FadeDirections.Out) ? 0 : 1;
        if (fadeDirection == FadeDirections.Out)
        {
            while (alpha >= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null;
            }
            fadeOutUIImage.enabled = false;
        }
        else
        {
            fadeOutUIImage.enabled = true;
            while (alpha <= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null;
            }
        }
    }
    #endregion

    #region HELPERS
    public IEnumerator FadeAndLoadScene(FadeDirections fadeDirection, string sceneToLoad)
    {
        yield return Fade(fadeDirection);
        if(sceneToLoad == "Exit")
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }


    private void SetColorImage(ref float alpha, FadeDirections fadeDirection)
    {
        fadeOutUIImage.color = new Color(fadeOutUIImage.color.r, fadeOutUIImage.color.g, fadeOutUIImage.color.b, alpha);
        alpha += Time.deltaTime * (1.0f / fadeSpeed) * ((fadeDirection == FadeDirections.Out) ? -1 : 1);
    }
    #endregion
}
