﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{


    #region Pause Menu, and some buttons will appear on the game over screen
    public Button btn_resume;
    public GameObject pauseMenu;
    #endregion

    #region Slider That's show the distance between the baby and the monster
    public Slider colorGradient;
    public Gradient gradient;
    public Image fill;
    #endregion

    #region GameOver Controllers and variables
    private bool gameOver = false;

    [SerializeField]
    private GameObject gameOverController;
    #endregion

    // Start is called before the first frame update
    void Start()

    {
        gradient.Evaluate(0f);
        //starts the corouting for the slider
        StartCoroutine(changeColorSlider());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            PausesHowButtons();
            Time.timeScale = 0.0f;
        }
    }


    //This will update the color everytime depending on the distance of the monster vs baby
    private IEnumerator changeColorSlider()
    {
        while (!gameOver)
        {
            fill.color = gradient.Evaluate(colorGradient.value);
            yield return new WaitForSeconds(.2f);
        }

    }


    //fazer funcao de game over
    //fazer a funcao de vencer

    public void setDistance(float dist)
    {
        colorGradient.value = dist;
    }

    public void resumeGame()
    {
        PauseHideButtons();
        Time.timeScale = 1.0f;
    }

    public void restartGame()
    {
        PauseHideButtons();
        Time.timeScale = 1.0f;
    }

    void PausesHowButtons()
    {
        pauseMenu.SetActive(true);
    }

    void PauseHideButtons()
    {
        pauseMenu.SetActive(false);
    }


    //function for the gameover

    public void GameOver(string how)
    {
        gameOver = true;
        
        if(how == "Monster"){
            gameOverController.GetComponent<GameOverScript>().DieByMonster();
        }
        else if (how == "Baby")
        {
            gameOverController.GetComponent<GameOverScript>().DieByBaby();
        }
        else if (how == "Win")
        {
            gameOverController.GetComponent<GameOverScript>().WinGame();
        }
    }
}
