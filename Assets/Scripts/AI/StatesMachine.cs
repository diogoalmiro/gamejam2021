﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatesMachine
{
    private GenericState currentState;
    
    public GenericState CurrentState {
        get => currentState;
    }

    public void Initialize(GenericState initialState){
        currentState = initialState;
        currentState.Enter();
    }

    public void Update()
    {
        if(currentState != null){
            GenericState nextState = currentState.Update();
            if (nextState != null)
            {
            ChangeState(nextState);
            }
        }
    }

    public void ChangeState(GenericState state){
        Debug.Log($"ChangeState: {currentState} ==> {state}");
        currentState.Exit();
        currentState = state;
        currentState.Enter();
    }
}
