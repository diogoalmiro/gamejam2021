using AI;
using UnityEngine;
using UnityEngine.AI;

//[CreateAssetMenu(fileName = "KillState", menuName = "GameJam/KillState", order = 0)]
public class StubState : GenericState {
    public StubState(Personality personality) : base(personality) {}

    public override void Enter(){
        agent.SetDestination(new Vector3(9.34f,0f,-45.06f));
    }
}