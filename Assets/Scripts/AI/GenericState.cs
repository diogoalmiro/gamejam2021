using AI;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.XR;

public abstract class GenericState
{
    // Convenience fields
    protected Personality personality;
    protected Transform transform;
    protected NavMeshAgent agent;

    public GenericState(Personality personality) {
        this.personality = personality;
        this.agent = personality.GetComponent<NavMeshAgent>();
        this.transform = personality.transform;
    }

    public virtual void Enter() {
        // To implement in subclasses
    }

    // runs every frame
    public virtual GenericState Update() {
        // To implement in subclasses
        return null;
    }

    public virtual void Exit() {
        // To implement in subclasses
    }

    protected bool IsReachingDestination()
    {
        return agent.remainingDistance < personality.distanceThreshold;
    }
}