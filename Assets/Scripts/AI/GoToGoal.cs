﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToGoal : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agent;
    public GameObject target;

    void Start()
    {
        agent.SetDestination(target.transform.position);
    }
}
