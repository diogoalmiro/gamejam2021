using UnityEngine;

namespace AI
{
    /// <summary>
    /// A personalidade vai ter uma memória, e podemos usar isto para dizer:
    ///  - Eu vi este indivíduo, vou atacar este indivíduo
    ///  - Eu ouvi este som aqui
    /// </summary>
    public class Personality : MonoBehaviour {

    

        public float distanceThreshold = 0.5f;

        private StatesMachine statesMachine;
    

        public void Awake() {
            statesMachine = new StatesMachine();
        }

        public virtual void Initialize(GenericState initialState)
        {
            statesMachine.Initialize(initialState);
        }

        private void Update() {
            statesMachine.Update();
        }

    
    
    }
}