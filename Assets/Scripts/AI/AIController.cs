using System;
using System.Collections;
using AI;
using AI.Baby_States;
using AI.Monster_States;
using UnityEngine;
using Unity;
using Random = UnityEngine.Random;

public class AIController : MonoBehaviour {
    
    #region Estados
    
    public enum BabyStates
    {
        BABY_ESCAPING,
        BABY_APPROACHING,
        BABY_VENTING,
        BABY_WIN,
        STUB
    }

    public enum MonsterStates
    {
        MONSTER_WANDERING,
        MONSTER_KILL,
        MONSTER_FOLLOWING,
        STUB
    }
    
    public GenericState StateFromEnum(MonsterStates statesEnum) {
        switch (statesEnum)
        {
            case MonsterStates.MONSTER_WANDERING:
                return new WanderingState(monster);
            case MonsterStates.MONSTER_KILL:
                return new KillState(monster);
            case MonsterStates.MONSTER_FOLLOWING:
                return new FollowingState(monster);
            case MonsterStates.STUB:
            default:
                return new StubState(baby /* we flipped a coin. It was tails. */);
        }
    }

    public GenericState StateFromEnum(BabyStates statesEnum)
    {
        switch (statesEnum)
        {
            case BabyStates.BABY_APPROACHING:
                return new ApproachingState(baby);
            case BabyStates.BABY_ESCAPING:
                return new EscapingState(baby);
            case BabyStates.BABY_VENTING:
                return new VentingState(baby);
            case BabyStates.BABY_WIN:
                return new StoppedState(baby);
            case BabyStates.STUB:
            default:
                return new StubState(baby /* we flipped a coin. It was tails. */);
        }
    }

    #endregion

    
    public GameObject[] AllBoxColliders { get; private set; }


    #region Inspector Properties

    [Header("Drag 'n Drop")]
    public BabyPersonality baby;
    public MonsterPersonality monster;
    public GameObject player;
    public GameController gameController;
    public AmplifySound audioController;

    [Header("MAGIC NUMBERS")]
    public float MAX_DIST = 14f;

    [Header("Initial states")]
    public BabyStates initialBabyState;
    public MonsterStates initialMonsterState;

    [Header("Baby parameters")]
    public float babyApproachingSpeed = 3.0f;
    public float babyEscapingSpeed = 6.0f;
    
    [Header("Monster parameters")]
    public float monsterFollowingSpeed = 7.0f;
    public float monsterWanderingSpeed = 3.5f;
    public float monsterKillingSpeed = 20.0f;

    #endregion
    
    public static AIController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogError("ESTÃO A SPAWNAR-ME PAREM COM ISSO");
        }
    }

    public void Start()
    {
        StartCoroutine(BabySounds());
        StartCoroutine(MonsterSounds());
        
        AllBoxColliders = GameObject.FindGameObjectsWithTag("Division triggers");
        baby.Initialize(StateFromEnum(initialBabyState));
        monster.Initialize(StateFromEnum(initialMonsterState));
    }

    private IEnumerator BabySounds()
    {
        while (true)
        {
            BabyCries();
            yield return new WaitForSeconds(Random.Range(5, 8));
        }
    }

    private IEnumerator MonsterSounds()
    {
        while (true)
        {
            MonsterRawrs();
            yield return new WaitForSeconds(Random.Range(5, 8));
        }
    }

    public void CallBaby()
    {
        baby.familiarNoise = player.transform;
    }

    public void MonsterRawrs()
    {
        baby.scaryNoise = monster.transform;
    }

    public void CallMonster()
    {
        monster.noiseLocation = baby.transform;
    }

    public void BabyCries()
    {
        monster.noiseLocation = baby.transform;
    }

    private void Update()
    {
        // Calcula a distancia entre o bebe e o monstro
        float babyMonsterDist = Vector3.Distance(baby.transform.position, monster.transform.position);
        if (gameController != null)
        {
            gameController.setDistance(1 - babyMonsterDist / MAX_DIST);
            if (Vector3.Distance(player.transform.position, baby.transform.position) < baby.distanceThreshold)
            {
                gameController.GameOver("Win");
            }
        }
        if (audioController != null)
        {
            audioController.setDistance(1 - babyMonsterDist / MAX_DIST);
        }


    }
}