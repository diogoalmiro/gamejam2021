using UnityEngine;
using Random = UnityEngine.Random;

//[CreateAssetMenu(fileName = "KillState", menuName = "GameJam/KillState", order = 0)]
namespace AI.Monster_States
{
    public class WanderingState : MonsterGenericState
    {
        private GameObject[] divisions;

        public WanderingState(MonsterPersonality personality) : base(personality)
        {
            divisions = AIController.Instance.AllBoxColliders;
        }

        public override void Enter()
        {
            // = new Color(128, 200, 255);
            int i = Random.Range(0, divisions.Length);
            GameObject randomRoom = divisions[i];
            agent.SetDestination(randomRoom.transform.position);
            agent.speed = AIController.Instance.monsterWanderingSpeed;
        }
    }
}