using UnityEngine;

//[CreateAssetMenu(fileName = "KillState", menuName = "GameJam/KillState", order = 0)]
namespace AI.Monster_States
{
    public class KillState : MonsterGenericState {
        public KillState(MonsterPersonality personality) : base(personality) { }

        public override void Enter()
        {
            // = Color.red;
            agent.SetDestination(monsterPersonality.killTarget.position);
            agent.speed = AIController.Instance.monsterKillingSpeed;
        }

        public override GenericState Update()
        {
            if (IsKillingTarget())
            {
                // TODO Game Over
                //return new WanderingState(monsterPersonality);            
                monsterPersonality.killTarget = null;
                AIController.Instance.gameController.GameOver("Monster");
                return new WanderingState(monsterPersonality);
            }
        
            if (IsReachingDestination())
            {
                monsterPersonality.killTarget = null;
                return new WanderingState(monsterPersonality);
            }

            return null;
        }
    }
}