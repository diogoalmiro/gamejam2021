using UnityEngine;

namespace AI.Monster_States
{
    public class FollowingState : MonsterGenericState
    {
        public FollowingState(MonsterPersonality personality) : base(personality) { }


        public override void Enter()
        {
            // = Color.blue;
            agent.SetDestination(monsterPersonality.noiseLocation.position);
            agent.speed = AIController.Instance.monsterFollowingSpeed;
            monsterPersonality.noiseLocation = null;
        }

    }
}