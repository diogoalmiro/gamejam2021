﻿using UnityEngine;

namespace AI.Monster_States
{
    public abstract class MonsterGenericState : GenericState
    {
        protected MonsterPersonality monsterPersonality => personality as MonsterPersonality;
        
        public MonsterGenericState(MonsterPersonality personality) : base(personality) { }

        public override GenericState Update()
        {
            // Se tivermos o bebe ou o jogador diretamente à nossa frente --> KILLING STATE
            var baby = AIController.Instance.baby.transform;

            if (HasEyeContact(baby))
            {
                Debug.Log("Monster made eye contact with BABY");
                monsterPersonality.killTarget = baby;
                return new KillState(monsterPersonality);
            }

            var player = AIController.Instance.player.transform;

            if (HasEyeContact(player))
            {
                Debug.Log("Monster made eye contact with PLAYER");
                monsterPersonality.killTarget = player;
                return new KillState(monsterPersonality);
            }
            
            // TODO Se receber um sinal sonoro ir para o FollowingState

            if (monsterPersonality.noiseLocation != null)
            {
                return new FollowingState(monsterPersonality);
            }
            
            if (IsReachingDestination())
            {
                // We want to wander to a new place
                return new WanderingState(monsterPersonality);
            }

            return null;
        }

        protected bool HasEyeContact(Transform entity)
        {
            Vector3 direction = (entity.transform.position - this.transform.position).normalized;
            float dot = Vector3.Dot(transform.forward, direction);

            Debug.DrawRay(transform.position, transform.forward * 8.0f, Color.red);
            Debug.DrawRay(transform.position, direction * 8.0f, Color.blue);

            // Se o bebe estiver dentro de um angulo de 155 graus à nossa frente
            if (dot > 0.0f)
            {
                if (Physics.Raycast(transform.position, direction, out var hit))
                {
                    Debug.Log($"Monster SAW {hit.transform.name}");
                    return hit.transform == entity.transform;
                }
                else
                {
                    Debug.LogError("wat");
                    Debug.Break();
                }
            }

            return false;
        }

        protected bool IsKillingTarget()
        {
            return Vector3.Distance(transform.position, monsterPersonality.killTarget.position) < monsterPersonality.distanceThreshold;
        }
    }
}