﻿using UnityEngine;

namespace AI.Monster_States
{
    public class MonsterPersonality : Personality
    {
        public Transform killTarget;
        public Transform noiseLocation;

        public void Initialize(MonsterGenericState initialState)
        {
            base.Initialize(initialState);
        }
    }
}