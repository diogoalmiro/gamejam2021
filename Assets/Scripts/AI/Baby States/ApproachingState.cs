using UnityEngine;

namespace AI.Baby_States
{
    public class ApproachingState : BabyGenericState
    {
        public ApproachingState(BabyPersonality personality) : base(personality)
        {
        }

        public override void Enter()
        {
            // = Color.green;
            agent.SetDestination(babyPersonality.familiarNoise.position);
            agent.speed = AIController.Instance.babyApproachingSpeed;
            babyPersonality.familiarNoise = null;
        }

        public override GenericState Update() =>
            base.Update() ?? (IsReachingDestination()
                ? new StoppedState(babyPersonality)
                : null);
    }
}