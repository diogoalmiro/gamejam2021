using UnityEngine;
using System.Linq;
using UnityEngine.AI;

namespace AI.Baby_States
{
    public class EscapingState : BabyGenericState {
        public EscapingState(BabyPersonality personality) : base(personality) { }

        public override void Enter()
        {
            var rooms = AIController.Instance.AllBoxColliders;
            var monster = babyPersonality.scaryNoise;
            float max = 0;
            Vector3 furthestRoom = transform.position;
            
            // Baby LINQ
            foreach (var p in rooms.Select(r => r.transform.position))
            {
                float distance = Vector3.Distance(monster.transform.position, p);
                if (distance > max)
                {
                    max = distance;
                    furthestRoom = p;
                }
            }

            agent.SetDestination(furthestRoom);
            agent.speed = AIController.Instance.babyEscapingSpeed;

            agent.areaMask = -2 << NavMesh.GetAreaFromName("Vent");
            agent.areaMask = NavMesh.AllAreas ^ (1 << NavMesh.GetAreaFromName("Vent"));
            agent.areaMask = NavMesh.AllAreas & ~(1 << NavMesh.GetAreaFromName("Vent"));

            babyPersonality.scaryNoise = null;
        }

        public override GenericState Update() =>
            base.Update() ?? (IsReachingDestination()
                ? new StoppedState(babyPersonality)
                : null);
    }
}