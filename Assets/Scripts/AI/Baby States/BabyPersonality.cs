﻿using UnityEngine;

namespace AI.Baby_States
{
    public class BabyPersonality : Personality
    {
        public Transform familiarNoise;
        public Transform scaryNoise;

        public void Initialize(BabyGenericState initialState)
        {
            base.Initialize(initialState);
        }
    }
}