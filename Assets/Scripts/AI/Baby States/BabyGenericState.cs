﻿namespace AI.Baby_States
{
    public abstract class BabyGenericState : GenericState
    {
        protected BabyPersonality babyPersonality => personality as BabyPersonality;
        protected BabyGenericState(BabyPersonality babyPersonality) : base(babyPersonality) { }
        
        public override GenericState Update()
        {
            if (babyPersonality.familiarNoise != null)
            {
                // Ouvimos um barulho familiar
                return new ApproachingState(babyPersonality);
            }
            
            if (babyPersonality.scaryNoise != null)
            {
                // Ouvimos um barulho assustador
                return new EscapingState(babyPersonality);
            }

            return null;
        }
    }
}