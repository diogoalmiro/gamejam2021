using UnityEngine;

namespace AI.Baby_States
{
    public class StoppedState : BabyGenericState {
        public StoppedState(BabyPersonality personality) : base(personality) { }

        public override void Enter()
        {
            // = Color.gray;
        }
        
    }
}