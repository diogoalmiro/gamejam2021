﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelperGui : MonoBehaviour
{
    public GameObject[] btns;
    public GameObject Story;
    public GameObject Credits;
    public float posX;
    void Start()
    {
        posX = btns[0].gameObject.transform.position.x;
    }

    public void toggleButtonsOffStory()
    {
        foreach(GameObject btn in btns)
        {
            iTween.MoveTo(btn.gameObject, iTween.Hash("position", new Vector3(Screen.width + 100,btn.transform.position.y, btn.transform.position.z), "time", .5f, "easetype", iTween.EaseType.linear));
        }
        iTween.MoveTo(Story.gameObject, iTween.Hash("position", new Vector3(Screen.width / 2, Screen.height / 2, 0), "time", 1f, "easetype", iTween.EaseType.easeInBounce));
    }

    public void toggleButtonsOnStory()
    {

        iTween.MoveTo(Story.gameObject, iTween.Hash(
            "position", new Vector3(Screen.width / 2, Screen.height + 300 , 0),
            "time", 1f,
            "easetype", iTween.EaseType.easeInBounce,
            "oncompletetarget", gameObject,
            "oncomplete","GetButtons"));
        
    }

    public void GetButtons()
    {
        foreach (GameObject btn in btns)
        {
            iTween.MoveTo(btn.gameObject, iTween.Hash("position", new Vector3(posX, btn.transform.position.y, btn.transform.position.z), "time", .5f, "easetype", iTween.EaseType.linear));
        }
    }

    public void toggleButtonsOffCredits()
    {
        foreach (GameObject btn in btns)
        {
            iTween.MoveTo(btn.gameObject, iTween.Hash("position", new Vector3(Screen.width + 100, btn.transform.position.y, btn.transform.position.z), "time", .5f, "easetype", iTween.EaseType.linear));
        }
        iTween.MoveTo(Credits.gameObject, iTween.Hash("position", new Vector3(Screen.width / 2, Screen.height / 2, 0), "time", 1f, "easetype", iTween.EaseType.easeInBounce));
    }

    public void toggleButtonsOnCredits()
    {

        iTween.MoveTo(Credits.gameObject, iTween.Hash(
            "position", new Vector3(Screen.width / 2, Screen.height + 300, 0),
            "time", 1f,
            "easetype", iTween.EaseType.easeInBounce,
            "oncompletetarget", gameObject,
            "oncomplete", "GetButtons"));

    }
}
