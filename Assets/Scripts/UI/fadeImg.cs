﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fadeImg : MonoBehaviour
{


    public float FadeRate;
    private Image image;
    private float targetAlpha;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        Material instantiatedMaterial = Instantiate<Material>(image.material);
        image.material = instantiatedMaterial;
        targetAlpha = image.material.color.a;
    }

    public IEnumerator FadeIn()
    {
        targetAlpha = 1.0f;
        Color curColor = image.color;
        while (Mathf.Abs(curColor.a - targetAlpha) > 0.0001f)
        {
            curColor.a = Mathf.Lerp(curColor.a, targetAlpha, FadeRate * Time.deltaTime);
            image.color = curColor;
            yield return null;
        }
        curColor.a = targetAlpha;
        image.color = curColor;
        GameObject.Find("LevelSelectController").GetComponent<LevelChanger>().StartNewGame();
    }
}
