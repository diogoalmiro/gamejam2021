﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpscareFlicker : MonoBehaviour
{
    private bool finishedScream = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(flicker());
    }

    private IEnumerator flicker()
    {
        int count = 0;
        while(count <= 100)
        {
            if(count % 2 == 0)
            {
                Color c = GetComponent<RawImage>().color;
                c.a = 1;
                GetComponent<RawImage>().color = c;
            }
            else
            {
                Color c = GetComponent<RawImage>().color;
                c.a = 0;
                GetComponent<RawImage>().color = c;
            }
            yield return new WaitForSeconds(.07f);
            count++;
        }

        finishedScream = true;
    }

    public bool getFinished()
    {
        return finishedScream;
    }
}
