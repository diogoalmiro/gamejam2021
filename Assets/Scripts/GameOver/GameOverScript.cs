﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class GameOverScript : MonoBehaviour
{
    #region Public Fields
    [Header("Prefab for the jumpscare when you die by the monster")]
    public GameObject MonsterPrefab;
    [Header("Prefab for the jumpscare when the monster kills the kid")]
    public GameObject BabyPrefab;
    [Header("Gameobject that will be used as parent for the prefabs ")]
    public GameObject PrefabsParent;
    [Header("Gameobject that will be used for the pause menu")]
    public GameObject pauseMenuGameobject;
    #endregion


    #region Death by Monster
    public void DieByMonster()
    {
        StartCoroutine(DeathByMonster());
    }

    private IEnumerator DeathByMonster()
    {
        GameObject instance_monster = Instantiate(MonsterPrefab, new Vector2(Screen.width / 2 + 90, 0), Quaternion.identity, PrefabsParent.transform);
        while (!instance_monster.GetComponent<JumpscareFlicker>().getFinished())
        {
            yield return new WaitForSeconds(1);
        }

        GameOverYeah("monster");

        //open the gameover screen and stuff
        
    }
    #endregion


    #region Death by Baby getting killed by monster
    public void DieByBaby()
    {
        StartCoroutine(DeathByBaby());
    }

    private IEnumerator DeathByBaby()
    {
        GameObject instance_baby = Instantiate(BabyPrefab, new Vector2(Screen.width / 2, Screen.height / 2), Quaternion.identity, PrefabsParent.transform);
        while (!instance_baby.GetComponent<JumpscareFlicker>().getFinished())
        {
            yield return new WaitForSeconds(1);
        }
        GameOverYeah("baby");

    }
    #endregion


    public void WinGame()
    {
        pauseMenuGameobject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "You Win!";
        pauseMenuGameobject.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Congratulations! You Found your baby!";
        pauseMenuGameobject.transform.GetChild(3).gameObject.SetActive(false);
        pauseMenuGameobject.SetActive(true);
    }

    private void GameOverYeah(string whoKill)
    {
        string message;
        if(whoKill == "monster")
        {
            message = "The monster saw You!";
        }
        else
        {
            message = "You were too late";
        }

        pauseMenuGameobject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "You Lost!";
        pauseMenuGameobject.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = message;
        pauseMenuGameobject.transform.GetChild(3).gameObject.SetActive(false);
        pauseMenuGameobject.SetActive(true);
    }

}